import { Component, OnInit } from '@angular/core';

interface UserData {
  name: string
  reviews: number
  desc: string
}

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
  
  Users: [UserData] = [
    {
      name: 'Cristofer Carder Junior',
      reviews: 234,
      desc: 'UI\UX Designer, Web Designer, Mobile App Designer, UI\UX Designer, Web Designer,Mobile App Designer'
    }
  ]

  ngOnInit(): void {
  }

}
